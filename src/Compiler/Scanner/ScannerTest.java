package Compiler.Scanner;

import java.io.IOException;
import static Compiler.Scanner.TokenType.*;

/**
  * Performs runs of the CMinusScanner to test its lexing.
  *
  * @author James Osborne and Jeremy Tiberg
  * File: ScannerTest.java
  * Created:  29 Jan 2018
  * Description: This test simply lexes a C- source with and without errors
  * and outputs the results to separate files.
  **/

public class ScannerTest {
    private static CMinusScanner scanner;
    private static Token token;
            
    public static void main(String[] args) throws IOException {
        for (Integer i = 0; i < 5; ++i) {
            try {
                token = new Token();
                System.out.print("\nTesting " + i + "\n\n");
                scanner = new CMinusScanner("test" + i + ".c-");

                while (token.getTokenType() != EOF_TOKEN && token.getTokenType() != ERROR_TOKEN) {
                    token = scanner.getNextToken();
                }
                
                if (token.getTokenType() == ERROR_TOKEN) {
                    throw new Exception("Erroneous token: " + token.getTokenData());
                }
                
                scanner.writeToOutputFile("sourceWithErrors.lex");
            }
            catch (Exception ex) {
                System.out.print(ex.getMessage() + "\n");
            }
            finally {
                continue;
            }
        }
    }
}

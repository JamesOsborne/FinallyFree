package Compiler.SyntaxParser.Statement;

import Compiler.SyntaxParser.Statement.Statement;
import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import Compiler.SyntaxParser.Expression.*;
import lowlevel.*;
import static lowlevel.Operand.OperandType.*;
import static lowlevel.Operation.OperationType.*;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: ReturnStatement.java
  **/
public class ReturnStatement extends Statement {
    private Expression returnExpression;
    
    public ReturnStatement() {
        this(null);
    }
    
    public ReturnStatement(Expression returnExpression) {
        this.returnExpression = returnExpression;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Return Statement {");
        
        printHelper(childIndent, "return");
        if (this.returnExpression != null) {
            this.returnExpression.print(childIndent);
        }
        printHelper(childIndent, ";");
        
        printHelper(indent, "}");
    }
    
    @Override
    public void genLLCode(Function function) {
        throw new LowLevelException("ReturnStatement must be provided with a return block");
    }
    
    @Override
    public void genLLCode(Function function, BasicBlock returnBlock) {
        genLLCode(function, returnBlock, null);
    }
        
    @Override
    public void genLLCode(Function function, BasicBlock returnBlock, BasicBlock basicBlock) {
        if (returnExpression != null) {
            BasicBlock currBlock = function.getCurrBlock();
            Operation rightHandSide = returnExpression.genLLCode(function);
            Operation returnStatement = new Operation(ASSIGN, currBlock);

            if (returnExpression instanceof NumExpression ||
                returnExpression instanceof VarExpression) {
                returnStatement.setSrcOperand(0, rightHandSide.getSrcOperand(0));
            } else if (returnExpression instanceof CallExpression) {
                returnStatement.setSrcOperand(0, new Operand(MACRO, "RetReg"));
            } else {
                returnStatement.setSrcOperand(0, rightHandSide.getDestOperand(0));
            }

            returnStatement.setDestOperand(0, new Operand(MACRO, "RetReg"));
            currBlock.appendOper(returnStatement);

            Operation jumpOperation = new Operation(JMP, currBlock);
            jumpOperation.setSrcOperand(0, new Operand(BLOCK, returnBlock.getBlockNum()));
            currBlock.appendOper(jumpOperation);
        }
    }
}

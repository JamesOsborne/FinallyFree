package Compiler.SyntaxParser;

/**
 *
 * @author James Osborne and Jeremy Tiberg
 * * File: CodeGenerationException.java
 */
public class CodeGenerationException extends RuntimeException {
    // Parameterless Constructor
    public CodeGenerationException() {}

    // Constructor that accepts a message
    public CodeGenerationException(String message) {
        super(message);
    }
}
